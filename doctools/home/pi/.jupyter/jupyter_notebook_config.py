import platform

c.NotebookApp.base_project_url = '/'+platform.node()+'/jupyter'
c.NotebookApp.open_browser = False
c.NotebookApp.port = 5010
c.NotebookApp.token = ''
c.NotebookApp.trust_xheaders = True
#c.NotebookApp.allow_origin = '*'
