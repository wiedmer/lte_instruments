class templateConfigure():
    def __init__(self,baseUrlList,fileUrl='.'):
        self.urlDictList=[]
        templateEnding='.template'
        self.fileUrl=fileUrl
        try:
            import instrumentConfig
            self.cfg=instrumentConfig.instrumentConfig()
            self.jupyter_url=self.cfg.jupyter["url"]
        except:
            self.jupyter_url='.'
        
        for baseUrl in baseUrlList:
            urlDict={}
            urlDict['baseUrl']=baseUrl
            if baseUrl.startswith('~'):
                urlDict['source']='/home'+baseUrl[1:]
            elif baseUrl.startswith('/'):
                urlDict['source']='/root'+baseUrl
            else:
                raise Exception("cannot get path from "+baseUrl)

            if urlDict['source'].endswith(templateEnding):
                urlDict['editFile']=urlDict['source'][0:-len(templateEnding)]
                urlDict['editTemplate']=urlDict['source']
                urlDict['dest']=urlDict['baseUrl'][0:-len(templateEnding)]
            else:
                urlDict['editFile']=urlDict['source']
                urlDict['dest']=urlDict['baseUrl']
            self.urlDictList.append(urlDict)
        
    def getCopyCmd(self,urlDict):  
        pass_arg=["sudo","cp",self.fileUrl+urlDict['editFile'],urlDict['dest']]
        #subprocess.check_call(pass_arg)
        return ' '.join(pass_arg)
    
    def getAllCopyCmd(self):
        for urlDict in self.urlDictList:
            print(self.getCopyCmd(urlDict))

    def render(self,urlDict):
        with open(self.fileUrl+urlDict['editTemplate']) as f:
            tmpl = Template(f.read())
        with open(self.fileUrl+urlDict['editFile'],"w") as f:
            tmpl = f.write(tmpl.render(
            cfg=self.cfg
        ))
            
    def renderAll(self):
        for urlDict in self.urlDictList:
            if 'editTemplate' in urlDict :
                self.render(urlDict)
                print("render "+urlDict['editFile'])
    
    def displayEditTable(self,editDir='/edit/lte_instruments/doctools'):
        from IPython.display import display, HTML
        from jinja2 import Template
        t = Template("""

        <table text-align= "center">
          <tr>
            <th>Name</th>
            <th>File</th> 
            <th>Template</th>
          </tr>

          {% for urlDict in urlDictList %}
          <tr>
            <td>  {{urlDict['baseUrl']}}</td>

            <td> <a href="{{editDir+urlDict['editFile']}}">File</a> </td>
            {%if urlDict['editTemplate']%}
            <td><a href="{{editDir+urlDict['editTemplate']}}">Template</a></td>
            {%else%}
            <td></td>
            {%endif%}
          </tr>
          {% endfor %}
        </table>
        """)
        display(HTML(t.render(urlDictList=self.urlDictList,editDir=editDir)))