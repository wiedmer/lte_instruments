import json, platform,os

class instrumentConfig():
    def __init__(self,cfgFile="~/lte_instruments/instruments.json"):
        cfgFile=os.path.expanduser(cfgFile)
        with open(cfgFile) as jsonFile:
            self._cfg=json.load(jsonFile)
            self.name=platform.node()
            for attr,value in self._cfg[self.name].items():
                setattr(self,attr, value)     
    def __getitem__(self, i):
        return self._cfg[i]
    
if __name__ == "__main__":
    cfg=instrumentConfig()
    print("Name: '"+ cfg.name + "'")
    print(json.dumps(cfg[cfg.name],indent=2,sort_keys=True))