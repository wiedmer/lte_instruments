import setuptools
from setuptools import setup

setup(name='instrumentConfig',
      version='0.1',
      description='instrumentConfig',
      author='Antoine Wiedmer',
      author_email='antoine.wiedmer@epfl.ch',
      packages=setuptools.find_packages(),
      zip_safe=False)
