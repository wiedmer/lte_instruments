import json
import datetime
import dateutil.parser
import sqlalchemy as db
from sqlalchemy import Column, Integer,Float, JSON, DateTime,Boolean,String,Text, Table,MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import math
Base = declarative_base()

class fineParticles(Base):
    __tablename__ = 'fineParticles'
    extend_existing=True
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    data = Column(Text)
    syncronized=Column(Boolean,default=False)
    
class pyranoRaw(Base):
    __tablename__ = 'pyranoRaw'
    extend_existing=True
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    instrument = Column(Text, primary_key=True)
    data = Column(Text)
    syncronized=Column(Boolean,default=False)
    
class rawData(Base):
    __tablename__ = 'rawData'
    extend_existing=True
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    instrument = Column(Text)
    data = Column(Text)
    syncronized=Column(Boolean,default=False)
    
    
class dbUtils():
    def __init__(self):
        self.engine = db.create_engine('mysql+pymysql://mesures:e2MduYwHQWuF@localhost/db')
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        #self.nameAlias={"5-1.6":"fp6","5-1.3":"fp3","5-1.4":"fp4","5-1.7.2":"fp9"}
        self.nameAlias={"5-1.3":"fp3","5-1.4":"fp4","5-1.5":"fp5","5-1.6":"fp6","5-1.7.4":"fp7","5-1.7.3":"fp8","5-1.7.2":"fp9","5-1.7.1":"fp10"}
        self.tab=self.createTableFineParticles(self.nameAlias,'particlesTable')
       
        self.pyranoNameList=['uPT100','uRref','visUEmf','irUEmf','irTb','visEsolar','irLnet','irLd']
        self.pyranoTab=self.createTablePyrano(self.pyranoNameList,'pyranoTable')

        
    
    def addRawMeasure(self,mesure):
        postDict=json.loads(mesure)
        mesDate=dateutil.parser.parse(postDict['ts'])
        
        print(postDict)
        
        if 'instrument' in postDict:
            print('add instrument')
            
            entry=rawData(date=mesDate,instrument=postDict['instrument'],data=json.dumps(postDict['data']))
            self.session.add(entry)
            self.session.commit()
            
        elif 'data' in postDict:
            entry=fineParticles(date=mesDate,data=json.dumps(postDict['data']))
            self.session.add(entry)


            newtab=self.tab()

            newtab.date=mesDate
            data=postDict['data']
            for i in self.nameAlias:
                try:
                    setattr(newtab,self.nameAlias[i]+'_pm10',data[i][0])
                    setattr(newtab,self.nameAlias[i]+'_pm25',data[i][1])
                except:
                    print("Could not add "+i+" ("+self.nameAlias[i]+")")
                     
        elif 'pyrano' in postDict:
            entry=pyranoRaw(date=mesDate,data=json.dumps(postDict['pyrano']))
            self.session.add(entry)


            newtab=self.pyranoTab()

            newtab.date=mesDate
            data=postDict['pyrano']
            
            rref=2170
            visS=15.07
            irS=11.46

            uPT100=data["uPT100"]
            uRref=data["uRref"]
            visUEmf=data["visUEmf"]
            irUEmf=data["irUEmf"]
            
            print("uPT100 %0.3fV, uRref %0.3fV ,visUEmf %0.3fmV ,irUEmf %0.3fmV "%(uPT100,uRref,visUEmf*1000,irUEmf*1000))

            try:
                RPT100=uPT100*rref/uRref
                #print(RPT100)
                alpha=3.9080e-3
                beta=-5.8019e-7
                irTb=(-alpha+math.sqrt(alpha**2-4*beta*(-RPT100/100+1)))/(2*beta)

            except:
                RPT100=-100
                irTb=-100

            visEsolar=visUEmf*1e6/visS
            irLnet=irUEmf*1e6/irS
            irLd=irLnet+5.67e-8*((irTb+273.15)**4)
            
            data["irTb"]=irTb
            data["visEsolar"]=visEsolar
            data["irLnet"]=irLnet
            data["irLd"]=irLd
            
            for pyranoName in self.pyranoNameList:
                try:
                    setattr(newtab,pyranoName,data[pyranoName])
                except:
                    print("Could not add "+pyranoName+" ("+pyranoName+")")     
                   
                    
            
        #self.session.add(newtab)
        self.session.commit()
        
    def createTablePyrano(self,nameList,name):
        class newTable(Base):
            __tablename__ = name
            extend_existing=True
            id = Column(Integer, primary_key=True)
            date = Column(DateTime)
            
        for i in range(len(nameList)):
            try:
                setattr(newTable,nameList[i],Column(Float))
            except:
                print("Could not add "+i+" ("+nameList[i]+")")
        Base.metadata.create_all(self.engine)
        self.session.commit()
        return newTable
        
    def createTableFineParticles(self,nameAlias,name):
        class newTable(Base):
            __tablename__ = name
            extend_existing=True
            id = Column(Integer, primary_key=True)
            date = Column(DateTime)
            
        for i in nameAlias:
            try:
                setattr(newTable,nameAlias[i]+'_pm10',Column(Float))
                setattr(newTable,nameAlias[i]+'_pm25',Column(Float))
            except:
                print("Could not add "+i+" ("+nameAlias[i]+")")
        Base.metadata.create_all(self.engine)
        self.session.commit()
        return newTable
