$(document).ready(function(){

var received = $('#received');

    
if (window.location.protocol==='https:'){
    var socketAdress="wss://"+window.location.host+window.location.pathname+"ws";
}
else{
    var socketAdress="ws://"+window.location.host+window.location.pathname+"ws";
}
console.log("socketAdress: "+ socketAdress);
var socket = new WebSocket(socketAdress)
socket.onopen = function(){  
  console.log("connected "+socketAdress);
}; 

socket.onmessage = function (message) {
    

    
    try {
        var data = JSON.parse(message.data);
        if("MRRPro6" in data){
            console.log(data["MRRPro6"])
            var i;
            for (i = 0; i < 4; i++) {
                if("l"+i.toString() in data["MRRPro6"]){
                    document.getElementById("MRRPro6_l"+i.toString()).innerHTML = data["MRRPro6"]["l"+i.toString()];
                }
            }
            if("l1" in data["MRRPro6"]){
                received.append(data["MRRPro6"]["l1"]);
                received.append($('<br/>'));
            }

            //$('#MRRPro6_l1').innerHTML=message.data;
        }
        else{
            received.append(message.data);
            received.append($('<br/>'));
        }
    } catch (e) {
        

    }
};

socket.onclose = function(){
  console.log("disconnected"); 
};

var sendMessage = function(message) {
  console.log("sending:" + JSON.stringify(message));
  socket.send(JSON.stringify(message));
};

// send a command to the serial port
$("#cmd_send").click(function(ev){
  ev.preventDefault();
  var cmd = $('#cmd_value').val();
  sendMessage({ 'data' : cmd,'feedback':true});
  $('#cmd_value').val("");
});

$('#clear').click(function(){
  received.empty();
});
});
