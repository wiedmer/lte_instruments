import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.gen
import tornado.autoreload
from dict_recursive_update import recursive_update

import json,os
from pymongo import MongoClient

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('static/index.html')

class MyWebSocketHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True
    def initialize(self,parent,clients,data):
        self.parent = parent
        self.clients=clients
        self.data=data
    def open(self):
        print('new connection')
        self.clients.append(self)
        print(json.dumps(self.data))
        self.write_message(json.dumps(self.data))

    def on_message(self, message):
        #print('tornado received from client: %s' % str(message))
        try:
            cmd=json.loads(message)
            cmd['client']=self
            self.parent.sendCommand(cmd)
        except ValueError:
            self.write_message(json.dumps({'error':'comand not in JSON format','cmd':message}))


    def on_close(self):
        print('connection closed')
        self.clients.remove(self)

class Data(tornado.web.RequestHandler):
    def initialize(self,parent,db):
        self.parent=parent
        self.db=db

    def post(self):
        print(self.request.body.decode("utf-8"))
        data=json.loads(self.request.body.decode("utf-8"))
        try:
            self.parent.writeToAll("Post: "+self.request.body.decode("utf-8"))
        except :
            raise tornado.web.HTTPError(400)
        print(str(data))
        #self.db.addRawMeasure(self.request.body.decode("utf-8"))
        self.write("OK")


class DataSever():
    def __init__(self,dbName,port=5000,address='',url=''):
        self.clients = []
        client = MongoClient()#**connOptions
        self.db = client[dbName]
        app = tornado.web.Application(
            handlers=[
                    (r"%s/"%url, IndexHandler),
                    (r"%s/static/(.*)"%url, tornado.web.StaticFileHandler, {'path':  './static/'}),
                    (r"%s/ws"%url, MyWebSocketHandler,dict(parent=self,clients=self.clients)),
                    (r"%s/putData"%url, Data,dict(parent=self,db=self.db)),
                ],debug=True)
        httpServer = tornado.httpserver.HTTPServer(app)
        httpServer.listen(port, address=address)
        print("Listening on port:", port)
        tornado.autoreload.start()
        for dir, _, files in os.walk('static'):
            [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]

        ## adjust the scheduler_interval according to the frames sent by the serial port
        scheduler_interval = 200
        io_loop = tornado.ioloop.IOLoop.current()
        #scheduler = tornado.ioloop.PeriodicCallback(self.actualizeStatus, scheduler_interval)
        #scheduler.start()
        io_loop.start()
    def sendCommand(self,command):
        self.writeToAll(command['data'])
        print(command)

    def writeToAll(self, message):
        for c in self.clients:
            c.write_message(message)



if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Data_Server')

    parser.add_argument('-p, --port',dest='port', type=int, default=5000,
                       help='Server port number (default=5000)')
    parser.add_argument('-a, --address',dest='address', default='',
                       help='allowed host (default='')')
    parser.add_argument('-d, --dbName',dest='dbName', default='testDatabase',
                           help='mongoDB database Name (default=testDatabase, example:"LTE")')
    parser.add_argument('-u, --url',dest='url', default='',
                       help='base url (default="/", example:"/data")')

    args = vars(parser.parse_args())

    print(args)
    dataServer=DataSever(**args)
