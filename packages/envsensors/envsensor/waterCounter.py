from .envsensor import envsensor
import serial
import meterbus

class WaterCounter(envsensor):
    def __init__(self,serialPort,address,name=None):
        envsensor.__init__(self,name=name)
        self.serialPort=serialPort
        self.address=address
        self.ser=None
    def measure(self):
        if not self.ser:
            self.ser=serial.Serial(self.serialPort, 2400, 8, 'E', 1, 0.5)
        try:
            valueDict=dict()
            for addr in self.address:
                meterbus.send_request_frame(self.ser, addr)

                loadData=meterbus.recv_frame(self.ser, 33)

                frame = meterbus.load(loadData)
                #assert isinstance(frame, meterbus.TelegramLong)

                #print(frame.to_JSON())
                valueDict[addr]=float(frame.records[1].value)
            return valueDict
        except:
            self.ser.close()
            self.ser=None
