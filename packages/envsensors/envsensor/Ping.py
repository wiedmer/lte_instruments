import platform
import subprocess
from .envsensor import envsensor

class Ping(envsensor):
    def __init__(self,ip,name=None):
        if name is None:
            name=ip
        envsensor.__init__(self,name=name)
        self.ip=ip
    def measure(self):
        param = '-n' if platform.system().lower()=='windows' else '-c'
        command = ['ping', param, '1', self.ip]
        return {'respond':subprocess.call(command) == 0}
