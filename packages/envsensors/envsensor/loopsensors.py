from .envsensor import envsensor
import threading
"""
class loopsensors(envsensor):
    def __init__(self,name=None,threaded=False,neasted=False):
        envsensor.__init__(self,name=name)
        self.sensorList=[]
        self.threaded=threaded
        self.neasted=neasted

    def addSensor(self,newSensor):
        self.sensorList.append(newSensor)

    def measure(self):
        actDict={}
        self.appendMeasure(actDict)
        return actDict
    def appendMeasure(self,actDict,neasted=False,*argv,**kwargs):
        if self.neasted:
            mesDict={}
            actDict[self.name]=mesDict
        else:
            mesDict=actDict
        if self.threaded:
            threads=[]
            for sensor in self.sensorList:
                newTh=threading.Thread(target=sensor.appendMeasure,args=(mesDict,argv),kwargs=kwargs)
                newTh.start()
                threads.append(newTh)
            for th in threads:
                th.join()
        else:
            for sensor in self.sensorList:
                actDict[sensor.name]=sensor.getMeasure(*argv,**kwargs)
"""

class loopsensors(envsensor):
    def __init__(self,name=None,threaded=False,neasted=False):
        envsensor.__init__(self,name=name)
        self.sensorList=[]
        self.threaded=threaded
        self.neasted=neasted

    def addSensor(self,newSensor):
        self.sensorList.append(newSensor)

    def measure(self,*argv,**kwargs):
        mesDict={}
        if self.threaded:
            threads=[]
            for sensor in self.sensorList:
                newTh=threading.Thread(target=sensor.appendMeasure,args=(mesDict,argv),kwargs=kwargs)
                newTh.start()
                threads.append(newTh)
            for th in threads:
                th.join()
        else:
            for sensor in self.sensorList:
                sensor.appendMeasure(mesDict,*argv,**kwargs)
        if self.neasted:
            return {self.name:mesDict}
        else:
            return mesDict
