import json

class envsensor:
    def __init__(self,name=None):
        if name:
            self.name=name
        else:
            self.name=self.__class__.__name__
        pass
        self.lastMeasure={}
    @staticmethod
    def getDiff(a,b):
        diffDict={}
        for itemName in b:
            if not itemName in a:
                diffDict[itemName]=b[itemName]
            elif a[itemName]==b[itemName]:
                continue
            else:
                if type(b[itemName]) is dict:
                    diffDict[itemName]=envsensor.getDiff(a[itemName],b[itemName])
                else:
                    diffDict[itemName]=b[itemName]
        return diffDict
    def getMeasure(self,new=True,diff=False,*argv,**kwargs):
        if new:
            newMes=self.measure(*argv,**kwargs)

            if diff:
                retMes=envsensor.getDiff(self.lastMeasure,newMes)
            else:
                retMes= newMes.copy()

            self.lastMeasure.update(retMes)
            return retMes
        else:
            return self.lastMeasure.copy()

    def measure(self):
        return {}
    def appendMeasure(self,actDict,new=True,*argv,**kwargs):
        actDict[self.name]=self.getMeasure(*argv,**kwargs)

class filterDict(envsensor):
    def __init__(self,directory,name=None):
        envsensor.__init__(self,name=name)
        self.directory=directory
