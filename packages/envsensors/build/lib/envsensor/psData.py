import psutil
from .envsensor import envsensor

class Temperature(envsensor):
    def measure(self):
        valueDict={}
        tempreturesSensor=psutil.sensors_temperatures()
        for sensorName in tempreturesSensor:
            valueDict[sensorName]=[i.current for i in tempreturesSensor[sensorName]]
        return valueDict

class Process(envsensor):
    def __init__(self,processName,name=None):
        envsensor.__init__(self,name=name)
        self.processName=processName
    def measure(self):
        return {self.processName:[p.info['pid'] for p in psutil.process_iter(attrs=['pid', 'name']) if p.info['name']==self.processName]}

class DiskUsage(envsensor):
    def __init__(self,directory,name=None):
        envsensor.__init__(self,name=name)
        self.directory=directory
    def measure(self):
        obj_Disk = psutil.disk_usage(self.directory)
        return {'used':obj_Disk.used,'free':obj_Disk.free}

class Memory(envsensor):
    def measure(self):
        return {
            'memory': {
                'used': psutil.virtual_memory().used,
                'free': psutil.virtual_memory().free,
                'percent': psutil.virtual_memory().percent
            },
            'swap': {
                'used': psutil.swap_memory().used,
                'free': psutil.swap_memory().free,
                'percent': psutil.swap_memory().percent
            }
        }
