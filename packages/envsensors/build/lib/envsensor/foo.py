
from .envsensor import envsensor
import random
import time
class foo(envsensor):
    def __init__(self,name=None,n=1,defDelay=0):
        envsensor.__init__(self,name=name)
        self.n=n
        self.actVals={}
        self.defDelay=defDelay
        for i in range(n):
            self.actVals["S%d"%i]=0.0

    def measure(self,delay=0):
        time.sleep(delay)
        time.sleep(self.defDelay)
        for s in self.actVals:
            self.actVals[s]+=random.random()
        return self.actVals
    def test(self):
        print('test')
