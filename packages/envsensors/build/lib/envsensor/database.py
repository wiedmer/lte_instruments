from pymongo import MongoClient
import json
from datetime import datetime

class mongoDB():
    def __init__(self,connOptions={},dbName='testDatabase',platformName=None):
        client = MongoClient(**connOptions)
        db = client[dbName]
        if platformName is None:
            import platform
            platformName=platform.node()
        self.collection = db[platformName]

    def insert(self,data,date=None,instrument=None,s=False):
        if not date:
            date=datetime.utcnow()
        self.collection.insert_one({'d':date,'i':instrument,'data':data,'s':s})

    def _unrolDict(self,inDict,outData=None,name='',indexList={}):
        if not outData:
            outData=dict()
        for entry in inDict:
            if name=='':
                nameB=entry
            else:
                nameB=name+'.'+entry
            if isinstance(inDict[entry],dict):
                self._unrolDict(inDict[entry],outData,nameB,indexList)
            else:
                outData[nameB]=inDict[entry]
                indexList[nameB]=None
        return outData

    def find(self,*args, **kwargs):
        args=list(args)
        while len(args)<2:
            args.append(dict())
        if args[1] is None:
            args[1]=dict()
        args[1]['_id']= False
            #args[1]['s']= False
        return list(self.collection.find(*args, **kwargs))

    def findAsUnroledDict(self,*args, **kwargs):
        tabData=[]
        indexList=dict()
        for data in self.find(*args, **kwargs):
            tabData.append(self._unrolDict(data,indexList=indexList))
        return tabData,indexList
