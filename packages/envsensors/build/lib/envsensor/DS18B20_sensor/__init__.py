class DS18B20_sensor():
    def getSensorList(self):
        file = open('/sys/devices/w1_bus_master1/w1_master_slaves')
        w1_slaves = file.readlines()
        sensorList=[line.split("\n")[0] for line in w1_slaves] 
        
        file.close()
        return sensorList
    
    def measure(self,sensorList=None):
        if sensorList is None:
            sensorList=self.getSensorList()
        # Fuer jeden 1-Wire Slave aktuelle Temperatur ausgeben
        retVals=[]
        for w1_slave in sensorList:
            try:
                # 1-wire Slave Datei lesen
                file = open('/sys/bus/w1/devices/' + str(w1_slave) + '/w1_slave')
                filecontent = file.read()
                file.close()

                # Temperaturwerte auslesen und konvertieren
                stringvalue = filecontent.split("\n")[1].split(" ")[9]
                retVals.append( float(stringvalue[2:]) / 1000)
            except:
                retVals.append(None)   
        return retVals

if __name__ == "__main__":
    temp=DS18B20_sensor()

    sensors= temp.getSensorList()
    temperatures=temp.measure(sensors)

    for i in range(len(sensors)):
        print(sensors[i]+" : "+str(temperatures[i])+"°C")
