import os, errno
import platform
import datetime
import json

class Logger():
    def __init__(self,instrument, platform=None,fileFolder=None,db=None):
        self.instrument=instrument
        if fileFolder:
            self.fileLogger=fileLogger(folder=fileFolder,instrument=instrument)
        else:
            self.fileLogger=None
        self.db=db

    def insert(self,data,date=None):
        if date is None:
            date=datetime.datetime.utcnow()

        if self.fileLogger:
            print('insert file '+str(data))
            self.fileLogger.insert(data,date)

        if self.db:
            self.db.insert(data,date,self.instrument)


class fileLogger():
    def __init__(self,folder,instrument):
        self.instrument=instrument
        self.fullname=platform.node()+"_"+instrument
        self.folder="%s%s/%s"%(folder,platform.node(),instrument)
        try:
            os.makedirs(self.folder)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def insert(self,data,date=None):
        if date is None:
            date=datetime.datetime.utcnow()

        try:
            logFile=open(self.folder+"/"+date.date().isoformat()+".log", 'a+')
            logFile.write(json.dumps([date.isoformat(),data])+"\n")
            logFile.close()
        except Exception as e:
            print("fileLogError:"+str(e))
